mod bar_modules;
mod message;
mod module;
mod module_worker;
pub mod prelude;
mod status_server;
mod system;
mod x11_utils;

#[macro_use]
extern crate lazy_static;

pub(crate) use system::SYSTEM;

pub type PauseBetweenYields = std::time::Duration;

/// This is the placeholder all format's have to contain!!!
pub const PLACEHOLDER: &'static str = "{VALUE}";

#[macro_export]
macro_rules! start_bar {
    ($config:expr) => {
        use dwm_statusbar_channels::prelude::*;

        fn main() {
            // Establish channel
            let (tx, rx) = channel();

            // Read config
            let config = $config;

            // Create workers
            let workers: Vec<ModuleWorker> = config
                .into_iter()
                .enumerate()
                .map(|(index, wrapper)| ModuleWorker::new(tx.clone(), index, wrapper))
                .collect();

            // Create server
            let server = StatusServer::new(workers.len(), " | ", rx);

            // make space for all the workers + 1 server
            let mut handles = Vec::with_capacity(workers.len() + 1);

            // Start up the workers
            workers
                .into_iter()
                .for_each(|worker| handles.push(worker.start()));

            // And start the server
            handles.push(server.start());

            // Await their completion
            handles.into_iter().for_each(|handle| {
                handle.join().unwrap();
            });
        }
    };
}

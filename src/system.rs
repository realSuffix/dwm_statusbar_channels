use systemstat::{Platform, System};
lazy_static! {
    pub static ref SYSTEM: System = System::new();
}

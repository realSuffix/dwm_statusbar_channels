use crate::message::UpdateMessage;
use crate::x11_utils::RootWindow;
use std::sync::mpsc::Receiver;
use std::thread::{spawn, JoinHandle};

pub struct StatusServer {
    pub root_window: RootWindow,
    pub segments: Vec<String>,
    pub delimiter: &'static str,
    pub receiver: Receiver<UpdateMessage>,
}

impl StatusServer {
    pub fn new(
        amount_modules: usize,
        delimiter: &'static str,
        receiver: Receiver<UpdateMessage>,
    ) -> Self {
        Self {
            root_window: RootWindow::init_window(),
            segments: vec![String::new(); amount_modules],
            delimiter,
            receiver,
        }
    }

    pub fn start(mut self) -> JoinHandle<()> {
        spawn(move || loop {
            let next_msg = self.receiver.recv();
            if let Ok(val) = next_msg {
                self.update_segments(val);
                self.update_bar();
            }
        })
    }

    fn update_segments(&mut self, val: UpdateMessage) {
        let UpdateMessage(value, index) = val;
        std::mem::replace(&mut self.segments[index], value);
    }

    fn update_bar(&self) {
        let content: String = self.segments.join(self.delimiter);
        self.root_window.update_bar(content);
    }
}

use crate::message::UpdateMessage;
use crate::module::*;
use crate::PauseBetweenYields;
use std::sync::mpsc::Sender;
use std::thread::*;

pub struct ModuleWorker {
    sender: Sender<UpdateMessage>,
    index: usize,
    module: Box<dyn Module>,
    pause: PauseBetweenYields,
}

impl ModuleWorker {
    pub fn new(sender: Sender<UpdateMessage>, index: usize, wrapper: ModuleWrapper) -> Self {
        let ModuleWrapper(module, pause) = wrapper;
        Self {
            sender,
            index,
            module,
            pause,
        }
    }

    pub fn start(mut self) -> JoinHandle<()> {
        spawn(move || loop {
            let content = self.module.yield_next_value();
            let message = UpdateMessage(content, self.index);
            self.sender.send(message).unwrap();
            std::thread::sleep(self.pause);
        })
    }
}
